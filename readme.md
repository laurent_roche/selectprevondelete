## Description
Selection of the previous message when deleting

## Description-FR
Selection du message précédent après avoir supprimé un message.

## Full Description-FR
Si comme moi, vous regardez vos message par ordre décroissant (du plus récent en haut au plus ancien en bas), lorsque vous lisez vos e-mails, vous voulez lire le plus ancien non lu et remonter.

Fréquemment, vous allez supprimer un message. Le comportement "standard" de Thunderbird est de passer au message suivant, mais dans le contexte donné précédemment, on veut passer au message précédent, pour lire le "prochain" message non lu.

C'est ce que fait cette extension Thunderbird, il suffit de la télécharger et l'activer : il n'y a rien à régler.

A noter que si vous voulez juste ne sélectionner aucun message après une suppression, il existe l'extension "deselect on delete tb78" (dont je me suis inspiré pour créer cette extension-ci).


## Build 
Create xpi file with command:   
`zip -r thunderbird-select-prev-on-delete.xpi *`
