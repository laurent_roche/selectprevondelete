/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var { Services } = ChromeUtils.import("resource://gre/modules/Services.jsm");
var { ExtensionSupport } = ChromeUtils.import("resource:///modules/ExtensionSupport.jsm");

// Boilerplate to listen for when to set up and tear down our add-on in the
// application's windows.

var selectprev = class extends ExtensionCommon.ExtensionAPI {
  getAPI(context) {
    context.callOnClose(this);
    return {
      selectprev: {
        init() {
          for (let win of Services.wm.getEnumerator(null)) {
            setupWindow(win);
          }
          ExtensionSupport.registerWindowListener("selectprevListener", {
            chromeURLs: [
              "chrome://messenger/content/messenger.xhtml",
              "chrome://messenger/content/messenger.xul",
            ],
            onLoadWindow(win) {
              setupWindow(win);
            },
          });
        },
      },
    };
  }

  close() {
    for (let win of Services.wm.getEnumerator(null)) {
      cleanupWindow(win);
    }
    ExtensionSupport.unregisterWindowListener("selectprevListener");
  }
};

var patchedThreadPaneCommandUpdater = new WeakMap();

function isWindowRelevant(win) {
  let windowtype = win.document.documentElement.getAttribute("windowtype");
  return windowtype === "mail:3pane";
}

function setupWindow(win) {
  let doSetup = (win) => {
    if (isWindowRelevant(win) && !patchedThreadPaneCommandUpdater.has(win)) {
      // TODO: Currently we only patch the first tab of each window, which is
      //       always an about:3pane tab. We probably have to set up a tabMonitor
      //       and need to look out for new/additional about:3pane tabs as well.

      // This is a pointer to the about:3pane of the first tab.
      let threadPaneCommandUpdater = win.gTabmail.tabInfo[0].chromeBrowser.contentWindow.dbViewWrapperListener.threadPaneCommandUpdater;

      // Patch
      threadPaneCommandUpdater.origUpdateNextMessageAfterDelete = threadPaneCommandUpdater.updateNextMessageAfterDelete;
      threadPaneCommandUpdater.updateNextMessageAfterDelete = function () {
        this.origUpdateNextMessageAfterDelete();
        // We assume we are IN the relevant tab and can use currentAbout3Pane to get the about:3pane
        // of the current tab. This will work for all mail tabs, not just the first one.
        let dbViewWrapperListener = win.gTabmail.currentAbout3Pane.dbViewWrapperListener;
        if (dbViewWrapperListener._nextViewIndexAfterDelete && dbViewWrapperListener._nextViewIndexAfterDelete > 0) {
          dbViewWrapperListener._nextViewIndexAfterDelete--;
        }
      };
      patchedThreadPaneCommandUpdater.set(win, threadPaneCommandUpdater);
    }
  };

  if (win.document.readyState == "complete") {
    doSetup(win);
  } else {
    win.addEventListener("load", function onload() {
      win.removeEventListener("load", onload);
      doSetup(win);
    });
  }
}

function cleanupWindow(win) {
  if (!isWindowRelevant(win))
    return;
  
  // Unpatch
  let threadPaneCommandUpdater = patchedThreadPaneCommandUpdater.get(win);
  threadPaneCommandUpdater.updateNextMessageAfterDelete = threadPaneCommandUpdater.origUpdateNextMessageAfterDelete;
  delete threadPaneCommandUpdater.origUpdateNextMessageAfterDelete;
  patchedThreadPaneCommandUpdater.delete(win);
}
